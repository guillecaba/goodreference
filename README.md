# Good Reference

Configuracion del envio de correo.


Modificar en  main_handler.php.


Con los siguientes valores por ejemplo.


```bash
    $mail->Host= 'mail.goodreference.com.py';
    $mail->Port = 587;
    $mail->Username='hola@goodreference.com.py';
    $mail->Password=''; poner la contraseña del correo
    $mail->SetFrom('hola@goodreference.com.py');
    $mail->AddAddress('julia@goodreference.com.py');
```

Deploy en un servidor nginx CentOS 7


Instalar Php
```bash
  sudo yum install php php-fpm
    
```


Configurar el procesador php

```bash
  sudo vi /etc/php.ini
    
```

En /etc/php.ini excerpt
```bash
  cgi.fix_pathinfo=0
    
```
luego 
```bash
  sudo vi /etc/php-fpm.d/www.conf
```
en el archivo que se abrio poner 
```bash
  
  listen = /var/run/php-fpm/php-fpm.sock
  listen.owner = nobody
  listen.group = nobody
  
  user = nginx
  group = nginx
  
    
```

Luego en el terminal 

```bash
 sudo systemctl start php-fpm
 sudo systemctl enable php-fpm
```

Configurar Nginx para Procesar Php pages
```bash
 sudo vi /etc/nginx/conf.d/default.conf
```

modificar en el archivo se se abrio
```bash
 server {
    listen       80;
    server_name  server_domain_name_or_IP;

    # note that these lines are originally from the "location /" block
    root   /usr/share/nginx/html;
    index index.php index.html index.htm;

    location / {
        try_files $uri $uri/ =404;
    }
    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
```

Guardar y recargar el servidor
```bash
sudo systemctl restart nginx
```

La guia completa esta en:
https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-on-centos-7

Que la fuerza te acompañe



